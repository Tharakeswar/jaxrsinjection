package com.jrsi.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/geoLocation")
public class GeoLocation {
	
	/*@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{zip:\\d+}")
	public String getGeoLocation(@PathParam("zip") String zip)
	{
		return "25,36.2";
	}*/
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{zip}")
	public String getGeoLocation(@PathParam("zip") int zip)
	{
		return "25,36.2";
	}

}
