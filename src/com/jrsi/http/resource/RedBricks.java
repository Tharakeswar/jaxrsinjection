package com.jrsi.http.resource;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

@Path("/redBricks")
public class RedBricks {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/header")
	public String getFlat(@Context HttpHeaders headers) {
		StringBuffer buffer = new StringBuffer();
		MultivaluedMap<String, String> headerParams = headers.getRequestHeaders();
		Map<String, Cookie> cookies = headers.getCookies();

		buffer.append("Header Param:").append(getMatrixParam(headerParams));

		buffer.append("    Cookies:");
		for (String key : cookies.keySet()) {
			Cookie cookie = cookies.get(key);
			buffer.append(cookie.getName() + "-").append(cookie.getValue());
		}
		return buffer.toString();

	}

	private String getMatrixParam(MultivaluedMap<String, String> map) {
		StringBuffer buffer = null;
		buffer = new StringBuffer();

		for (String key : map.keySet()) {
			buffer.append(key);

			List<String> values = map.get(key);
			for (String value : values) {
				buffer.append("-" + value + ",");
			}
			buffer.append(";");
		}
		return buffer.toString();
	}

}
