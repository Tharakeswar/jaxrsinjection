package com.jrsi.mvm.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

@Path("/redBricks")
public class RedBricks {
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/body")
	@Consumes(MediaType.TEXT_PLAIN)
	public String addFlat(MultivaluedMap<String, String> body)
	{
		StringBuffer buffer=new StringBuffer();
		
		buffer.append(getMatrixParam(body));
		
		return buffer.toString();
	}

	private String getMatrixParam(MultivaluedMap<String, String> map) {
		StringBuffer buffer = null;
		buffer = new StringBuffer();

		for (String key : map.keySet()) {
			buffer.append(key);

			List<String> values = map.get(key);
			for (String value : values) {
				buffer.append("-" + value + ",");
			}
			buffer.append(";");
		}
		return buffer.toString();
	}

}
