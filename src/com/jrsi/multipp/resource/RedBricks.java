package com.jrsi.multipp.resource;

import java.util.List;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

@Path("/redBricks/{city}")
public class RedBricks {

	/*
	 * @GET
	 * 
	 * @Produces(MediaType.TEXT_PLAIN)
	 * 
	 * @Path("/search/{propertyType}/{city}") public String
	 * search(@PathParam("propertyType") String
	 * propertyType, @QueryParam("location") String location,
	 * 
	 * @PathParam("city") String city) { return "propertyType-" + propertyType +
	 * "  location-" + location + "  city-" + city; }
	 */

	private static final int String = 0;

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	// @Path("/search:{propertyType}-{city}")
	// @Path("/search/{propertyType}/{location}/{city}")
	@Path("/search/{propertyType}")
	public String search(@PathParam("propertyType") String propertyType, @QueryParam("location") String location,
			@PathParam("city") String city) {
		return "propertyType-" + propertyType + "  location-" + location + "  city-" + city;
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	// @Path("/search/{propertyType}/{city}")
	@Path("/search/{propertyType}/{location}")
	public String search(@PathParam("propertyType") String propertyType, @QueryParam("location") String location,
			@QueryParam("minAmount") @DefaultValue("9999") double minAmount, @QueryParam("maxAmount") double maxAmount,
			@PathParam("city") String city) {
		return "propertyType-" + propertyType + "  location-" + location + " minAmount-" + minAmount + " maxAmount-"
				+ maxAmount + "  city-" + city;

	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/search/{propertyType}/{location}")
	public String search(@PathParam("propertyType") String propertyType, @PathParam("location") String location,
			@PathParam("city") String city, @MatrixParam("facing") String facing) {
		return "propertyType-" + propertyType + "  location-" + location + "  city-" + city + " facing-" + facing;
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/search/{propertyType}/{city}/{location}")
	public String search(@PathParam("propertyType") PathSegment propertyTypeSegment,
			@PathParam("location") PathSegment locationSegment, @PathParam("city") PathSegment citysegment) {
		MultivaluedMap<String, String> propertyMap = null;
		StringBuffer buffer = new StringBuffer();

		propertyMap = propertyTypeSegment.getMatrixParameters();
		buffer.append("propertyType :").append(propertyTypeSegment.getPath()).append("MatrixParam: ")
				.append(getMatrixParam(propertyMap)).append(" location: ").append(locationSegment.getPath())
				.append(" MatrixPram:").append(getMatrixParam(locationSegment.getMatrixParameters())).append(" City: ")
				.append(citysegment.getPath()).append("MatrixParam: ")
				.append(getMatrixParam(citysegment.getMatrixParameters()));

		return buffer.toString();
	}

	private String getMatrixParam(MultivaluedMap<String, String> map) {
		StringBuffer buffer = new StringBuffer();

		for (String key : map.keySet()) {

			buffer.append(key);

			for (String value : map.get(key))
			{
				// value = map.getFirst(key);
				buffer.append("-" + value).append(",");
			}
			buffer.append(";");
		}

		/*
		 * for (String keyMap : map.keySet()) { List<String> keys =
		 * map.get(keyMap); for (String key : keys) {
		 * buffer.append(key).append(":"); for (String value : map.get(key)) //
		 * value = map.getFirst(key); buffer.append("," + value).append(";"); }
		 * }
		 */
		return buffer.toString();
	}

}
