package com.jrsi.cookie.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/cookie")
public class RedBricksProperties {

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String addLand(@FormParam("facing") String facing, @FormParam("area") String area,
			@FormParam("surveyNo") String surveyNo,@CookieParam("cookie") String cookie) {
		return "facing " + facing + " area " + area + " surveyNo " + surveyNo + " cookie " + cookie;
	}

}
