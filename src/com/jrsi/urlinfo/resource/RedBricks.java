package com.jrsi.urlinfo.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriInfo;

@Path("/urlInfo")
public class RedBricks {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/search/{area}/{city}")
	public String searchFlat(@Context UriInfo uriInfo)
	{
		StringBuffer buffer=new StringBuffer();
		MultivaluedMap<String, String> queryMap = uriInfo.getQueryParameters();
		MultivaluedMap<String, String> pathMap = uriInfo.getPathParameters();
		List<PathSegment> pathSegments = uriInfo.getPathSegments(); 
		
		buffer.append("Query Param:").append(getMatrixParam(queryMap));
		buffer.append("\n");
		buffer.append("Path Param:").append(getMatrixParam(pathMap));
		buffer.append("\n");
		buffer.append("Matrix Param:");
		for(PathSegment pg: pathSegments)
		{
			buffer.append(pg.getPath()).append("-");
			MultivaluedMap<String,String> matrixParameters = pg.getMatrixParameters();
			buffer.append(getMatrixParam(matrixParameters));
		}
		return buffer.toString();
	}
	
	
	private String getMatrixParam(MultivaluedMap<String, String> map)
	{
		StringBuffer buffer=null;
		buffer=new StringBuffer();
		
		for(String key:map.keySet())
		{
			buffer.append(key);
			
			List<String> values = map.get(key);
		       for (String value : values) {
		    	   buffer.append("-"+value+",");
      		}
		       buffer.append(";");
		}
		return buffer.toString();
	}

}
