package com.jrsi.header.resource;

import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/flat")
public class RedBricksProperties {

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String addFlat(@FormParam("apartmentName") String apartmentName, @FormParam("floor") int floor,
			@FormParam("facing") String facing, @FormParam("area") String area, @FormParam("flatType") String flatType,
			@HeaderParam("accessCode") String accessCode) {
		return "apartmentName " + apartmentName + " floor " + floor + " facing " + facing + " area" + area
				+ " flatType " + flatType + " accessCode " + accessCode;
	}

}
