package com.jrsi.atc.resource;

public class AwbNo {
	private String branchCode;
	private String serialNo;
	
	public AwbNo(String in)
	{
		branchCode=in.substring(0,4);
		serialNo=in.substring(4,in.length());
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	@Override
	public String toString() {
		return "AwbNo [branchCode=" + branchCode + ", serialNo=" + serialNo + "]";
	}
	
	

}
