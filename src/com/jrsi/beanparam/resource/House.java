package com.jrsi.beanparam.resource;

import javax.ws.rs.MatrixParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public class House {
	
	@PathParam("housename")
	private String houseName;
	
	@QueryParam("area")
	private String area;
	
	@QueryParam("stairs")
	private int stairs;
	
	@PathParam("sqft")
	private int sqft;
	
	@MatrixParam("surveyNo")
	private int surveyNo;
	
	@PathParam("city")
	private String city;
	public String getHouseName() {
		return houseName;
	}
	public void setHouseName(String houseName) {
		this.houseName = houseName;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public int getStairs() {
		return stairs;
	}
	public void setStairs(int stairs) {
		this.stairs = stairs;
	}
	public int getSqft() {
		return sqft;
	}
	public void setSqft(int sqft) {
		this.sqft = sqft;
	}
	public int getSurveyNo() {
		return surveyNo;
	}
	public void setSurveyNo(int surveyNo) {
		this.surveyNo = surveyNo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "House [houseName=" + houseName + ", area=" + area + ", stairs=" + stairs + ", sqft=" + sqft
				+ ", surveyNo=" + surveyNo + ", city=" + city + "]";
	}
	
	
	

}
