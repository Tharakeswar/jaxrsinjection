package com.jrsi.formp.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/properties")
public class RedBricksProperties {

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String addFlat(@FormParam("apartmentName") String apartmentName, @FormParam("floor") int floor,
			@FormParam("facing") String facing, @FormParam("area") String area,
			@FormParam("flatType") String flatType) {
		return "apartmentName " + apartmentName + " floor " + floor + " facing " + facing + " area" + area
				+ " flatType " + flatType;
	}

}
